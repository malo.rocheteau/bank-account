package mrocheteau.bankaccount.domain;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public class AccountOperation {

    private final OperationType type;
    private final Instant date;
    private final BigDecimal amount;
    private final BigDecimal balance;
    private final String userDescription;

    public AccountOperation(final OperationType type, final Instant date, final BigDecimal amount,
                            final BigDecimal balance, final String userDescription) {

        this.amount = amount;
        this.type = type;
        this.date = date;
        this.balance = balance;
        this.userDescription = Objects.requireNonNullElse(userDescription, "");
    }

    public OperationType getType() {
        return this.type;
    }

    public Instant getDate() {
        return this.date;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    public String getUserDescription() {
        return this.userDescription;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final AccountOperation that = (AccountOperation) o;
        return this.type == that.type &&
                Objects.equals(this.date, that.date) &&
                Objects.equals(this.amount, that.amount) &&
                Objects.equals(this.balance, that.balance) &&
                Objects.equals(this.userDescription, that.userDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.type, this.date, this.amount, this.balance, this.userDescription);
    }

    @Override
    public String toString() {
        return "AccountOperation{" +
                "type=" + this.type +
                ", date=" + this.date +
                ", amount=" + this.amount +
                ", balance=" + this.balance +
                ", userDescription='" + this.userDescription + '\'' +
                '}';
    }
}

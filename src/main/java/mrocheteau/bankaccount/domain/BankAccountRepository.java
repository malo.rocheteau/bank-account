package mrocheteau.bankaccount.domain;

import java.util.List;
import java.util.Optional;

public interface BankAccountRepository {

    Optional<AccountOperation> findLastOperation();

    void addOperation(AccountOperation operation);

    List<AccountOperation> findAllOperations();
}

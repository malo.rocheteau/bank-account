package mrocheteau.bankaccount.domain;

import java.util.List;

public interface AccountStatementPrinter {
    void print(List<AccountOperation> operations);
}

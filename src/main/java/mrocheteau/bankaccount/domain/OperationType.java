package mrocheteau.bankaccount.domain;

public enum OperationType {
    DEPOSIT, WITHDRAWAL
}

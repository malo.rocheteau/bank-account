package mrocheteau.bankaccount.service;

/**
 * Status of a bank account response.
 */
public enum ResponseStatus {
    OK, ERROR
}

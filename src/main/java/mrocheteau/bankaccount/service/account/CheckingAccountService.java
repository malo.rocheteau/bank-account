package mrocheteau.bankaccount.service.account;

import mrocheteau.bankaccount.domain.AccountOperation;
import mrocheteau.bankaccount.domain.AccountStatementPrinter;
import mrocheteau.bankaccount.domain.BankAccountRepository;
import mrocheteau.bankaccount.domain.OperationType;
import mrocheteau.bankaccount.service.Response;
import mrocheteau.bankaccount.service.ResponseStatus;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.util.List;

public class CheckingAccountService implements CheckingAccount {

    static final String DEPOSIT_MESSAGE_OK = "%.2f deposited on checking account, new balance is %.2f";
    static final String DEPOSIT_ERROR_MESSAGE_NEGATIVE_AMOUNT =
            "A deposit of %.2f is not allowed (strictly positive only).";

    static final String WITHDRAWAL_MESSAGE_OK = "%.2f withdrawn from checking account, new balance is %.2f";
    static final String WITHDRAWAL_ERROR_MESSAGE_NEGATIVE_AMOUNT =
            "A withdrawal of %.2f is not allowed (must be strictly positive only).";
    static final String WITHDRAWAL_ERROR_MESSAGE_NOT_ENOUGH_MONEY =
            "A withdrawal of %.2f is not allowed (account balance is %.2f so it is not enough).";

    static final String FETCH_STATEMENT_MESSAGE_OK = "Account statement fetched successfully.";

    private static final String WITHDRAWAL = "WITHDRAWAL";
    private static final String DEPOSIT = "DEPOSIT";

    private final BankAccountRepository bankAccountRepository;
    private final Clock clock;

    public CheckingAccountService(final BankAccountRepository bankAccountRepository, final Clock clock) {
        this.bankAccountRepository = bankAccountRepository;
        this.clock = clock;
    }

    @Override
    public Response<AccountOperation> deposit(final BigDecimal amount, final String userDescription) {

        final BigDecimal scaledAmount = amount.setScale(2, RoundingMode.HALF_UP);

        if (scaledAmount.compareTo(BigDecimal.ZERO) <= 0) {
            return new Response<>(
                    DEPOSIT,
                    ResponseStatus.ERROR,
                    String.format(DEPOSIT_ERROR_MESSAGE_NEGATIVE_AMOUNT, scaledAmount),
                    null
            );
        }

        final BigDecimal newBalance = this.bankAccountRepository.findLastOperation()
                .map(AccountOperation::getBalance)
                .map(currentBalance -> currentBalance.add(scaledAmount))
                .orElse(scaledAmount);

        final AccountOperation depositOperation = new AccountOperation(
                OperationType.DEPOSIT,
                this.clock.instant(),
                scaledAmount,
                newBalance,
                userDescription
        );

        this.bankAccountRepository.addOperation(depositOperation);

        return new Response<>(
                DEPOSIT,
                ResponseStatus.OK,
                String.format(DEPOSIT_MESSAGE_OK, scaledAmount, newBalance),
                depositOperation
        );
    }


    @Override
    public Response<AccountOperation> withdraw(final BigDecimal amount, final String userDescription) {

        final BigDecimal scaledAmount = amount.setScale(2, RoundingMode.HALF_UP);

        if (scaledAmount.compareTo(BigDecimal.ZERO) <= 0) {
            return new Response<>(
                    WITHDRAWAL,
                    ResponseStatus.ERROR,
                    String.format(WITHDRAWAL_ERROR_MESSAGE_NEGATIVE_AMOUNT, scaledAmount),
                    null);
        }

        final BigDecimal currentBalance = this.bankAccountRepository.findLastOperation()
                .map(AccountOperation::getBalance)
                .orElse(BigDecimal.ZERO);

        if (scaledAmount.compareTo(currentBalance) > 0) {
            final String message = String.format(WITHDRAWAL_ERROR_MESSAGE_NOT_ENOUGH_MONEY, scaledAmount, currentBalance);

            return new Response<>(WITHDRAWAL, ResponseStatus.ERROR, message,null);
        }

        final BigDecimal newBalance = currentBalance.subtract(scaledAmount);

        final AccountOperation withdrawalOperation = new AccountOperation(
                OperationType.WITHDRAWAL,
                this.clock.instant(),
                scaledAmount,
                newBalance,
                userDescription
        );

        this.bankAccountRepository.addOperation(withdrawalOperation);

        return new Response<>(
                WITHDRAWAL,
                ResponseStatus.OK,
                String.format(WITHDRAWAL_MESSAGE_OK, scaledAmount, newBalance),
                withdrawalOperation
        );
    }

    @Override
    public void printStatement(final AccountStatementPrinter accountStatementPrinter) {
        final List<AccountOperation> operations = this.bankAccountRepository.findAllOperations();
        accountStatementPrinter.print(operations);
    }
}

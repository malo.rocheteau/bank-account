package mrocheteau.bankaccount.service.account;

import mrocheteau.bankaccount.domain.AccountOperation;
import mrocheteau.bankaccount.service.Response;

import java.math.BigDecimal;

public interface WithdrawableAccount {

    /**
     * Makes a withdrawal of the given amount from the bank account, if possible.
     *
     * @param amount          the amount to withdraw from the bank account.
     * @param userDescription the description set by the user about his withdrawal operation
     * @return a response containing the amount that remains on the bank account after the withdrawal, or a null
     * amount if it fails.
     */
    Response<AccountOperation> withdraw(BigDecimal amount, String userDescription);
}

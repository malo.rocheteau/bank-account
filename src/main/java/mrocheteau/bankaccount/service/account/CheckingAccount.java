package mrocheteau.bankaccount.service.account;

public interface CheckingAccount extends DepositableAccount, WithdrawableAccount, PrintableAccount {
}

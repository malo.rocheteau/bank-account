package mrocheteau.bankaccount.service.account;

import mrocheteau.bankaccount.domain.AccountStatementPrinter;

public interface PrintableAccount {

    void printStatement(final AccountStatementPrinter accountStatementPrinter);
}

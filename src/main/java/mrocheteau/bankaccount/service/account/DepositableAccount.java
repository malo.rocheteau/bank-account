package mrocheteau.bankaccount.service.account;

import mrocheteau.bankaccount.domain.AccountOperation;
import mrocheteau.bankaccount.service.Response;

import java.math.BigDecimal;

public interface DepositableAccount {

    /**
     * Makes a deposit of the given amount on the bank account.
     *
     * @param amount the amount to deposit on the bank account.
     * @param userDescription the description set by the user about his deposit operation
     * @return a response containing the new balance on the bank account, or a null amount if it failed.
     */
    Response<AccountOperation> deposit(BigDecimal amount, String userDescription);
}

package mrocheteau.bankaccount.service;

import java.util.Objects;
import java.util.Optional;

public class Response<T> {

    private final ResponseStatus status;
    private final String message;
    private final T data;

    public Response(final String action, final ResponseStatus status, final String message,
                    final T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public ResponseStatus getStatus() {
        return this.status;
    }

    public String getMessage() {
        return this.message;
    }

    public Optional<T> getData() {
        return Optional.ofNullable(this.data);
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final Response<?> response = (Response<?>) o;
        return this.status == response.status &&
                Objects.equals(this.message, response.message) &&
                Objects.equals(this.data, response.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.status, this.message, this.data);
    }

    @Override
    public String toString() {
        return "Response{" +
                "status=" + this.status +
                ", message='" + this.message + '\'' +
                ", data=" + this.data +
                '}';
    }
}

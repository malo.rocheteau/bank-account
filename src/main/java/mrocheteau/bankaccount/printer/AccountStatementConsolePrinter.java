package mrocheteau.bankaccount.printer;

import mrocheteau.bankaccount.domain.AccountOperation;
import mrocheteau.bankaccount.domain.AccountStatementPrinter;
import mrocheteau.bankaccount.domain.OperationType;

import java.math.RoundingMode;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class AccountStatementConsolePrinter implements AccountStatementPrinter {

    private final DateTimeFormatter dateFormatter;

    public AccountStatementConsolePrinter(final Locale locale, final ZoneId zone, final String dateFormatPattern) {

        this.dateFormatter = DateTimeFormatter.ofPattern(dateFormatPattern)
                .withLocale(locale)
                .withZone(zone);
    }

    @Override
    public void print(final List<AccountOperation> operations) {

        final String statement = operations.stream()
                .sorted(Comparator.comparing(AccountOperation::getDate).reversed())
                .map(this::operationToPrintableString)
                .collect(this.joinLinesCollector());

        System.out.println(statement);
    }

    private Collector<CharSequence, ?, String> joinLinesCollector() {

        final String firstLine = String.format("|| %-13s|| %-13s|| %-13s|| %-13s|| %-13s||" + System.lineSeparator(),
                "date", "credit", "debit", "balance", "description");

        return Collectors.joining(System.lineSeparator(), firstLine, "");
    }

    private String operationToPrintableString(final AccountOperation operation) {

        final String date = this.dateFormatter.format(operation.getDate());
        final String depositAmount =
                operation.getType() == OperationType.DEPOSIT ? operation.getAmount().toString() : "";
        final String withdrawalAmount =
                operation.getType() == OperationType.WITHDRAWAL ? operation.getAmount().toString() : "";
        final String balance = operation.getBalance().setScale(2, RoundingMode.HALF_UP).toString();
        final String description = operation.getUserDescription();

        return String.format("|| %-13s|| %-13s|| %-13s|| %-13s|| %-13s||", date, depositAmount, withdrawalAmount,
                balance, description);
    }
}
package acceptance;

import mrocheteau.bankaccount.domain.AccountOperation;
import mrocheteau.bankaccount.domain.BankAccountRepository;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class InMemoryBankAccountRepository implements BankAccountRepository {

    private final LinkedList<AccountOperation> operations;

    InMemoryBankAccountRepository() {
        this.operations = new LinkedList<>();
    }

    @Override
    public Optional<AccountOperation> findLastOperation() {
        return this.operations.isEmpty()
                ? Optional.empty()
                : Optional.of(this.operations.getLast());
    }

    @Override
    public void addOperation(final AccountOperation operation) {
        this.operations.add(Objects.requireNonNull(operation));
    }

    @Override
    public List<AccountOperation> findAllOperations() {
        return Collections.unmodifiableList(this.operations);
    }
}

package acceptance;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

/**
 * A Clock that tick 1 second for each call on instant() method.
 */
public class FakeClock extends Clock {

    private final ZoneId zone;
    private Instant now;

    public FakeClock(final Instant from, final ZoneId zone) {
        this.now = from;
        this.zone = zone;
    }

    @Override
    public ZoneId getZone() {
        return this.zone;
    }

    @Override
    public Clock withZone(final ZoneId newZone) {
        return new FakeClock(this.now, newZone);
    }

    @Override
    public Instant instant() {
        this.now = this.now.plus(1, ChronoUnit.DAYS);
        return Instant.from(this.now);
    }
}

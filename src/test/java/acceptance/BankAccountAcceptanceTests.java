package acceptance;

import mrocheteau.bankaccount.domain.AccountStatementPrinter;
import mrocheteau.bankaccount.domain.BankAccountRepository;
import mrocheteau.bankaccount.printer.AccountStatementConsolePrinter;
import mrocheteau.bankaccount.service.account.CheckingAccount;
import mrocheteau.bankaccount.service.account.CheckingAccountService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BankAccountAcceptanceTests {

    private static final Instant START_DATE_USED_FOR_TESTS = Instant.parse("2019-05-28T12:00:00.00Z");

    private ByteArrayOutputStream mockedOutputStream;
    private PrintStream consoleOutputStream;
    private Clock clock;
    private BankAccountRepository bankAccountRepository;
    private AccountStatementPrinter accountStatementPrinter;
    private CheckingAccount account;

    @BeforeEach
    void setUp() {
        // redirect output stream
        this.consoleOutputStream = System.out;
        this.mockedOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(this.mockedOutputStream));

        // prepare dependencies
        this.bankAccountRepository = new InMemoryBankAccountRepository();
        this.accountStatementPrinter = new AccountStatementConsolePrinter(Locale.FRANCE, ZoneId.of("UTC"),
            "dd/MM/yyyy");
        this.clock = new FakeClock(START_DATE_USED_FOR_TESTS, ZoneId.of("UTC"));

        // prepare service with dependencies injection
        this.account = new CheckingAccountService(this.bankAccountRepository, this.clock);
    }

    @AfterEach
    void restoreOutputStream() {
        System.setOut(this.consoleOutputStream);
    }

    @Test
    void kataUserStory() {
        final String expectedOutput =
            "|| date         || credit       || debit        || balance      || description  ||" + System.lineSeparator() +
                "|| 31/05/2019   ||              || 9.00         || 2.00         || Fourth       ||" + System.lineSeparator() +
                "|| 30/05/2019   || 1.00         ||              || 11.00        || Third        ||" + System.lineSeparator() +
                "|| 29/05/2019   || 10.00        ||              || 10.00        || First        ||" + System.lineSeparator();

        this.account.deposit(BigDecimal.TEN, "First");            // OK
        this.account.withdraw(BigDecimal.valueOf(20), "Second");  // Fail: not enough money
        this.account.deposit(BigDecimal.ONE, "Third");            // OK
        this.account.withdraw(BigDecimal.valueOf(9), "Fourth");   // OK
        this.account.deposit(BigDecimal.valueOf(-5), "Fifth");    // Fail: negative value
        this.account.deposit(BigDecimal.valueOf(0), "Sixth");     // Fail: zero value
        this.account.withdraw(BigDecimal.valueOf(-5), "Seventh"); // Fail: negative value
        this.account.withdraw(BigDecimal.valueOf(0), "Eight");    // Fail: zero value

        this.account.printStatement(this.accountStatementPrinter);

        final String actualOutput = new String(this.mockedOutputStream.toByteArray());
        assertEquals(expectedOutput, actualOutput);
    }

}

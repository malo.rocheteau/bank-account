package mrocheteau.bankaccount.printer;

import mrocheteau.bankaccount.domain.AccountOperation;
import mrocheteau.bankaccount.domain.AccountStatementPrinter;
import mrocheteau.bankaccount.domain.OperationType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountStatementConsolePrinterTest {

    private AccountStatementPrinter converter;
    private PrintStream originalStandardOutput;
    private ByteArrayOutputStream spiedStandardOutputStream;


    private static Stream<Arguments> provideArgs() {

        final Instant fixedInstant = Instant.parse("2012-01-14T12:00:00.00Z");

        final Arguments simpleOperations = Arguments.of(
                        "|| date         || credit       || debit        || balance      || description  ||" + System.lineSeparator() +
                        "|| 13/01/2012   ||              || 9.00         || 2.00         || Third        ||" + System.lineSeparator() +
                        "|| 12/01/2012   || 1.00         ||              || 11.00        || Second       ||" + System.lineSeparator() +
                        "|| 11/01/2012   || 10.00        ||              || 10.00        || First        ||" + System.lineSeparator(),
                Arrays.asList(
                        new AccountOperation(
                                OperationType.DEPOSIT, fixedInstant.minus(2, ChronoUnit.DAYS),
                                BigDecimal.ONE.setScale(2, RoundingMode.HALF_UP),
                                BigDecimal.valueOf(11).setScale(2,
                                        RoundingMode.HALF_UP),
                                "Second"
                        ),
                        new AccountOperation(
                                OperationType.DEPOSIT, fixedInstant.minus(3, ChronoUnit.DAYS),
                                BigDecimal.TEN.setScale(2, RoundingMode.HALF_UP),
                                BigDecimal.TEN.setScale(2, RoundingMode.HALF_UP),
                                "First"
                        ),
                        new AccountOperation(
                                OperationType.WITHDRAWAL,
                                fixedInstant.minus(1, ChronoUnit.DAYS),
                                BigDecimal.valueOf(9).setScale(2, RoundingMode.HALF_UP),
                                BigDecimal.valueOf(2).setScale(2, RoundingMode.HALF_UP),
                                "Third"
                        )
                )
        );

        final Arguments emptyOperations = Arguments.of(
                "|| date         || credit       || debit        || balance      || description  ||" + System.lineSeparator()
                        + System.lineSeparator(),
                new ArrayList<AccountOperation>()
        );


        return Stream.of(simpleOperations, emptyOperations);
    }

    @BeforeEach
    void setUp() {
        this.converter = new AccountStatementConsolePrinter(Locale.FRANCE, ZoneId.of("UTC"), "dd/MM/yyyy");

        // redirect standard output
        this.originalStandardOutput = System.out;
        this.spiedStandardOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(this.spiedStandardOutputStream));
    }


    @ParameterizedTest
    @DisplayName("It should return a string containing operations information, sorted by date (most recent first).")
    @MethodSource("provideArgs")
    void testConvertStatement(final String expectedOutput, final List<AccountOperation> inputOperations) {

        this.converter.print(inputOperations);
        assertEquals(expectedOutput, this.spiedStandardOutputStream.toString());
    }

    @AfterEach
    void restoreStandardOutput() {
        System.setOut(this.originalStandardOutput);
    }
}
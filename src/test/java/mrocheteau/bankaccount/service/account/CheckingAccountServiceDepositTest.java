package mrocheteau.bankaccount.service.account;

import mrocheteau.bankaccount.domain.AccountOperation;
import mrocheteau.bankaccount.domain.BankAccountRepository;
import mrocheteau.bankaccount.domain.OperationType;
import mrocheteau.bankaccount.service.Response;
import mrocheteau.bankaccount.service.ResponseStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@DisplayName("Tests for deposit operation on CheckingAccountService.")
class CheckingAccountServiceDepositTest {

    private Clock fixedClock;
    private BankAccountRepository bankAccountRepository;
    private DepositableAccount checkingAccountService;

    private static Stream<Arguments> provideArgsForDepositOperations() {
        return Stream.of(
                Arguments.of(
                        BigDecimal.valueOf(1.0).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.valueOf(50.0).setScale(2, RoundingMode.HALF_UP)),
                Arguments.of(
                        BigDecimal.valueOf(56.51).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.valueOf(50.0).setScale(2, RoundingMode.HALF_UP)),
                Arguments.of(
                        BigDecimal.valueOf(56.51).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.valueOf(56.51).setScale(2, RoundingMode.HALF_UP)),
                Arguments.of(
                        BigDecimal.valueOf(Double.MAX_VALUE).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)),
                Arguments.of(
                        BigDecimal.valueOf(Double.MAX_VALUE).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.valueOf(50.0).setScale(2, RoundingMode.HALF_UP))
        );
    }

    @BeforeEach
    void setUp() {
        this.bankAccountRepository = mock(BankAccountRepository.class);
        this.fixedClock = Clock.fixed(Instant.now(), ZoneId.of("UTC"));
        this.checkingAccountService = new CheckingAccountService(this.bankAccountRepository, this.fixedClock);
    }

    @ParameterizedTest
    @DisplayName("Given a strictly positive amount, it should send a deposit with the new balance and return this" +
            " operation.")
    @MethodSource("provideArgsForDepositOperations")
    void testDepositPositiveAmount(final BigDecimal inputAmount, final BigDecimal startBalance) {

        final BigDecimal expectedBalance = startBalance.add(inputAmount);
        final String userDescription = "Input user description.";

        final AccountOperation expectedOperation = new AccountOperation(
                OperationType.DEPOSIT,
                this.fixedClock.instant(),
                inputAmount,
                expectedBalance,
                userDescription
        );
        final Response<AccountOperation> expectedResponse = new Response<>(
                "DEPOSIT",
                ResponseStatus.OK,
                String.format(CheckingAccountService.DEPOSIT_MESSAGE_OK, inputAmount, expectedBalance),
                expectedOperation
        );
        final AccountOperation lastOperation = new AccountOperation(
                OperationType.DEPOSIT,
                this.fixedClock.instant().minus(1, ChronoUnit.DAYS),
                new BigDecimal(10),
                startBalance,
                "Some previous & useless description."
        );

        when(this.bankAccountRepository.findLastOperation()).thenReturn(Optional.of(lastOperation));
        doNothing().when(this.bankAccountRepository).addOperation(any(AccountOperation.class));

        final Response<AccountOperation> actualResponse =
                this.checkingAccountService.deposit(inputAmount, userDescription);

        final ArgumentCaptor<AccountOperation> operationCaptor = ArgumentCaptor.forClass(AccountOperation.class);
        verify(this.bankAccountRepository).findLastOperation();
        verify(this.bankAccountRepository).addOperation(operationCaptor.capture());
        verifyNoMoreInteractions(this.bankAccountRepository);

        final AccountOperation operationCaptured = operationCaptor.getValue();
        assertEquals(expectedOperation, operationCaptured);
        assertEquals(expectedResponse, actualResponse);
    }

    @ParameterizedTest
    @DisplayName("When deposit is the first operation, then start balance should be 0 and the end balance should " +
            "equal the amount.")
    @ValueSource(doubles = {1.0, 64.541564, Double.MAX_VALUE})
    void testDepositFirstOperation(final double doubleInputAmount) {

        final BigDecimal inputAmount = BigDecimal.valueOf(doubleInputAmount).setScale(2, RoundingMode.HALF_UP);
        final String userDescription = "Some new useless description.";

        final AccountOperation expectedOperation = new AccountOperation(
                OperationType.DEPOSIT,
                this.fixedClock.instant(),
                inputAmount,
                inputAmount,
                userDescription
        );
        final Response<AccountOperation> expectedResponse = new Response<>(
                "DEPOSIT",
                ResponseStatus.OK,
                String.format(CheckingAccountService.DEPOSIT_MESSAGE_OK, inputAmount, BigDecimal.ZERO.add(inputAmount)),
                expectedOperation
        );

        // "This is the first operation" means "findLastOperation() returns an empty Optional"
        when(this.bankAccountRepository.findLastOperation())
                .thenReturn(Optional.empty());

        final Response<AccountOperation> actualResponse =
                this.checkingAccountService.deposit(inputAmount, userDescription);

        verify(this.bankAccountRepository).findLastOperation();
        verify(this.bankAccountRepository).addOperation(any(AccountOperation.class));
        verifyNoMoreInteractions(this.bankAccountRepository);

        assertEquals(expectedResponse, actualResponse);
    }

    @ParameterizedTest
    @DisplayName("Given an amount <= 0, when calling deposit operation, it should return an error response and " +
            "never call the repository.")
    @ValueSource(doubles = {0.0, -1.0, -0.000001, -54.446464, -Double.MAX_VALUE})
    void testDepositZeroOrNegativeAmount(final double inputAmountDouble) {
        final BigDecimal inputAmount = BigDecimal.valueOf(inputAmountDouble).setScale(2, RoundingMode.HALF_UP);

        final Response<AccountOperation> expectedResponse = new Response<>(
                "DEPOSIT",
                ResponseStatus.ERROR,
                String.format(CheckingAccountService.DEPOSIT_ERROR_MESSAGE_NEGATIVE_AMOUNT, inputAmount),
                null
        );

        final Response<AccountOperation> actualResponse =
                this.checkingAccountService.deposit(inputAmount, "Some useless description");

        verifyZeroInteractions(this.bankAccountRepository);

        assertEquals(expectedResponse, actualResponse);
    }
}

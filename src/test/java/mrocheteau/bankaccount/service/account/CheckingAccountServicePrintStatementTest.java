package mrocheteau.bankaccount.service.account;

import mrocheteau.bankaccount.domain.AccountOperation;
import mrocheteau.bankaccount.domain.AccountStatementPrinter;
import mrocheteau.bankaccount.domain.BankAccountRepository;
import mrocheteau.bankaccount.domain.OperationType;
import mrocheteau.bankaccount.printer.AccountStatementConsolePrinter;
import mrocheteau.bankaccount.service.Response;
import mrocheteau.bankaccount.service.ResponseStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@DisplayName("Tests for print statement operation on CheckingAccountService.")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CheckingAccountServicePrintStatementTest {

    private final Clock fixedClock = Clock.fixed(Instant.parse("2012-01-14T12:00:00.00Z"), ZoneId.of("UTC"));
    private BankAccountRepository bankAccountRepository;
    private CheckingAccountService checkingAccountService;
    private AccountStatementPrinter accountStatementPrinter;

    @BeforeEach
    void setUp() {
        this.bankAccountRepository = mock(BankAccountRepository.class);
        this.accountStatementPrinter = mock(AccountStatementConsolePrinter.class);
        this.checkingAccountService = new CheckingAccountService(this.bankAccountRepository, this.fixedClock);
    }


    private Stream<Arguments> provideArgsForPrintStatement() {

        final Instant fixedDate = this.fixedClock.instant();

        final Arguments simpleOperations = Arguments.of(
                Arrays.asList(
                        new AccountOperation(OperationType.DEPOSIT, fixedDate.minus(3, ChronoUnit.DAYS),
                                BigDecimal.TEN, BigDecimal.TEN, "First deposit."),
                        new AccountOperation(OperationType.DEPOSIT, fixedDate.minus(2, ChronoUnit.DAYS),
                                BigDecimal.ONE, BigDecimal.valueOf(11), "Second deposit."),
                        new AccountOperation(OperationType.WITHDRAWAL, fixedDate.minus(1, ChronoUnit.DAYS),
                                BigDecimal.valueOf(9), BigDecimal.valueOf(2), "First withdrawal.")
                )
        );

        final Arguments emptyOperations = Arguments.of(new ArrayList<>());

        return Stream.of(simpleOperations, emptyOperations);
    }


    @ParameterizedTest
    @DisplayName("It should return the list of all operations done on checking account.")
    @MethodSource("provideArgsForPrintStatement")
    void testGetStatement(final List<AccountOperation> operations) {

        final Response<List<AccountOperation>> expectedStatement = new Response<>(
               "FETCH STATEMENT",
                ResponseStatus.OK,
                CheckingAccountService.FETCH_STATEMENT_MESSAGE_OK,
                null
        );

        when(this.bankAccountRepository.findAllOperations()).thenReturn(operations);
        doNothing().when(this.accountStatementPrinter).print(anyList());

        this.checkingAccountService.printStatement(this.accountStatementPrinter);

        @SuppressWarnings("unchecked") final ArgumentCaptor<List<AccountOperation>> argumentCaptor =
                ArgumentCaptor.forClass(List.class);
        verify(this.bankAccountRepository).findAllOperations();
        verify(this.accountStatementPrinter).print(argumentCaptor.capture());
        verifyNoMoreInteractions(this.bankAccountRepository);

        assertEquals(operations, argumentCaptor.getValue());
    }
}

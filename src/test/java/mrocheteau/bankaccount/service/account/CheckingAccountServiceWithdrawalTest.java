package mrocheteau.bankaccount.service.account;

import mrocheteau.bankaccount.domain.AccountOperation;
import mrocheteau.bankaccount.domain.BankAccountRepository;
import mrocheteau.bankaccount.domain.OperationType;
import mrocheteau.bankaccount.service.Response;
import mrocheteau.bankaccount.service.ResponseStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@DisplayName("Tests for withdrawal operation on CheckingAccountService.")
class CheckingAccountServiceWithdrawalTest {

    private Clock fixedClock;
    private BankAccountRepository bankAccountRepository;
    private WithdrawableAccount checkingAccountService;

    private static Stream<Arguments> provideArgsForWithdrawalOperations() {
        return Stream.of(
                Arguments.of(
                        BigDecimal.valueOf(1.0).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.valueOf(50.0).setScale(2, RoundingMode.HALF_UP)
                ),
                Arguments.of(
                        BigDecimal.valueOf(50.0).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.valueOf(56.51).setScale(2, RoundingMode.HALF_UP)
                ),
                Arguments.of(
                        BigDecimal.valueOf(56.51).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.valueOf(56.51).setScale(2, RoundingMode.HALF_UP)
                ),
                Arguments.of(
                        BigDecimal.valueOf(Double.MAX_VALUE).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.valueOf(Double.MAX_VALUE).setScale(2, RoundingMode.HALF_UP)
                ),
                Arguments.of(
                        BigDecimal.valueOf(50.0).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.valueOf(Double.MAX_VALUE).setScale(2, RoundingMode.HALF_UP)
                )
        );
    }

    @BeforeEach
    void setUp() {
        this.bankAccountRepository = mock(BankAccountRepository.class);
        this.fixedClock = Clock.fixed(Instant.now(), ZoneId.of("UTC"));
        this.checkingAccountService =
                new CheckingAccountService(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository,
                        CheckingAccountServiceWithdrawalTest.this.fixedClock);
    }

    @ParameterizedTest
    @DisplayName("Given a strictly positive amount, it should send a withdrawal with the new balance and return " +
            "this operation.")
    @MethodSource("provideArgsForWithdrawalOperations")
    void testWithdrawalPositiveAmount(final BigDecimal inputAmount, final BigDecimal startBalance) {

        final BigDecimal expectedBalance = startBalance.subtract(inputAmount);
        final String userDescription = "Some new useless description.";

        final AccountOperation expectedOperation = new AccountOperation(
                OperationType.WITHDRAWAL,
                CheckingAccountServiceWithdrawalTest.this.fixedClock.instant(),
                inputAmount,
                expectedBalance,
                userDescription
        );
        final Response<AccountOperation> expectedResponse = new Response<>(
                "WITHDRAWAL",
                ResponseStatus.OK,
                String.format(CheckingAccountService.WITHDRAWAL_MESSAGE_OK, inputAmount, expectedBalance),
                expectedOperation
        );

        final AccountOperation lastOperation = new AccountOperation(
                OperationType.DEPOSIT,
                CheckingAccountServiceWithdrawalTest.this.fixedClock.instant().minus(1, ChronoUnit.DAYS),
                new BigDecimal(10),
                startBalance,
                "A previous useless description."
        );

        when(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository.findLastOperation()).thenReturn(Optional.of(lastOperation));
        doNothing().when(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository).addOperation(any(AccountOperation.class));

        final Response<AccountOperation> actualResponse =
                this.checkingAccountService.withdraw(inputAmount, userDescription);

        final ArgumentCaptor<AccountOperation> operationCaptor = ArgumentCaptor.forClass(AccountOperation.class);
        verify(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository).findLastOperation();
        verify(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository).addOperation(operationCaptor.capture());
        verifyNoMoreInteractions(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository);

        final AccountOperation operationCaptured = operationCaptor.getValue();
        assertEquals(expectedOperation, operationCaptured);
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    @DisplayName("When withdrawal is the first operation, balance is 0 so it should send and error.")
    void testWithdrawalFirstOperation() {

        final BigDecimal inputAmount = new BigDecimal(10);
        final String userDescription = "Some new useless description.";

        final Response<AccountOperation> expectedResponse = new Response<>(
                "WITHDRAWAL",
                ResponseStatus.ERROR,
                String.format(CheckingAccountService.WITHDRAWAL_ERROR_MESSAGE_NOT_ENOUGH_MONEY, inputAmount,
                        BigDecimal.ZERO),
                null
        );

        // "This is the first operation" means "findLastOperation() returns an empty Optional"
        when(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository.findLastOperation())
                .thenReturn(Optional.empty());

        final Response<AccountOperation> actualResponse =
                this.checkingAccountService.withdraw(inputAmount, userDescription);

        verify(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository).findLastOperation();
        verifyNoMoreInteractions(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository);

        assertEquals(expectedResponse, actualResponse);
    }

    @ParameterizedTest
    @DisplayName("Given an amount <= 0, when calling withdrawal operation, it should return an error response and" +
            " never call the repository.")
    @ValueSource(doubles = {0.0, -1.0, -0.000001, -54.446464, -Double.MAX_VALUE})
    void testWithdrawalZeroOrNegativeAmount(final double inputAmountDouble) {
        final BigDecimal inputAmount = BigDecimal.valueOf(inputAmountDouble).setScale(2, RoundingMode.HALF_UP);

        final Response<AccountOperation> expectedResponse = new Response<>(
                "WITHDRAWAL",
                ResponseStatus.ERROR,
                String.format(CheckingAccountService.WITHDRAWAL_ERROR_MESSAGE_NEGATIVE_AMOUNT, inputAmount),
                null
        );

        final Response<AccountOperation> actualResponse =
                this.checkingAccountService.withdraw(inputAmount, "Some useless description");

        verifyZeroInteractions(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository);
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    @DisplayName("Given an amount greater than current balance, when calling withdrawal operation, it should " +
            "return an error response and never send any operation.")
    void testWithdrawalNotEnoughMoney() {
        final BigDecimal startBalance = BigDecimal.valueOf(10.0).setScale(2, RoundingMode.HALF_UP);
        final BigDecimal inputAmount = BigDecimal.valueOf(20.0).setScale(2, RoundingMode.HALF_UP);

        final AccountOperation lastOperation = new AccountOperation(
                OperationType.DEPOSIT,
                CheckingAccountServiceWithdrawalTest.this.fixedClock.instant().minus(1, ChronoUnit.DAYS),
                new BigDecimal(5),
                startBalance,
                "A previous useless description."
        );
        final Response<AccountOperation> expectedResponse = new Response<>(
                "WITHDRAWAL",
                ResponseStatus.ERROR,
                String.format(CheckingAccountService.WITHDRAWAL_ERROR_MESSAGE_NOT_ENOUGH_MONEY, inputAmount,
                        startBalance),
                null
        );

        when(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository.findLastOperation())
                .thenReturn(Optional.of(lastOperation));

        final Response<AccountOperation> actualResponse =
                this.checkingAccountService.withdraw(inputAmount, "Some useless description");

        verify(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository).findLastOperation();
        verifyNoMoreInteractions(CheckingAccountServiceWithdrawalTest.this.bankAccountRepository);
        assertEquals(expectedResponse, actualResponse);
    }
}

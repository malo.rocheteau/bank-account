package mrocheteau.bankaccount.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountOperationTest {

    private static Stream<Arguments> provideArgsForConstructorTest() {
        return Stream.of(
                Arguments.of(OperationType.DEPOSIT, Instant.now(), BigDecimal.ZERO, BigDecimal.ZERO, "Some description"),
                Arguments.of(OperationType.WITHDRAWAL, Instant.now(), BigDecimal.valueOf(50), BigDecimal.valueOf(125.546456), "Some description"),
                Arguments.of(null, null, null, null, "")
        );
    }


    @ParameterizedTest
    @DisplayName("It should create an object with accessibles properties.")
    @MethodSource("provideArgsForConstructorTest")
    void testConstructorOperation(final OperationType operationType, final Instant date, final BigDecimal amount,
                                  final BigDecimal endBalance, final String description) {

        final AccountOperation operation = new AccountOperation(operationType, date, amount, endBalance, description);

        assertEquals(operationType, operation.getType());
        assertEquals(date, operation.getDate());
        assertEquals(amount, operation.getAmount());
        assertEquals(endBalance, operation.getBalance());
        assertEquals(description, operation.getUserDescription());
    }

    @Test
    @DisplayName("Given a null description, it should create an operation with an empty string description.")
    void testNullDescription() {
        final AccountOperation operation = new AccountOperation(
                OperationType.DEPOSIT,
                Instant.now(),
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                null
        );

        assertEquals("", operation.getUserDescription());
    }

    @Test
    @DisplayName("Test for equals() method.")
    void testEqualsContract() {
        EqualsVerifier.forClass(AccountOperation.class)
                .usingGetClass()
                .verify();
    }
}
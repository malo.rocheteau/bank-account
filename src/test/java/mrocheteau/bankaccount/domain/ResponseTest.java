package mrocheteau.bankaccount.domain;


import mrocheteau.bankaccount.service.Response;
import mrocheteau.bankaccount.service.ResponseStatus;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class ResponseTest {

    private static Stream<Arguments> provideArgsForConstructorTest() {
        return Stream.of(
        Arguments.of(
                "DEPOSIT",
                ResponseStatus.OK,
                "    ",
                new BigDecimal(50)
        ),
        Arguments.of(
                "WITHDRAWAL",
                ResponseStatus.ERROR,
                "Text on \n multiple\n lines\n\nand some\ttabs.",
                new BigDecimal(150.64)
        ),
        Arguments.of(
                "FETCH STATEMENT",
                ResponseStatus.OK,
                "Your operation worked fine.",
                BigDecimal.ZERO)
        );
    }

    @ParameterizedTest
    @DisplayName("It should create an object with accessibles properties.")
    @MethodSource("provideArgsForConstructorTest")
    void testConstructor(final String action, final ResponseStatus status, final String message,
                         final BigDecimal someData) {

        final Response<?> response = new Response<>(action, status, message, someData);

        assertEquals(status, response.getStatus());
        assertEquals(message, response.getMessage());
        assertTrue(response.getData().isPresent());
        assertEquals(someData, response.getData().get());
    }

    @Test
    @DisplayName("Given no operation, when creating a response, it should not provide any operation.")
    void testGetOperationIsNotPresent() {
        final Response<?> response =
                new Response<>("DEPOSIT", ResponseStatus.OK, "Some message.", null);

        assertTrue(response.getData().isEmpty());

    }

    @Test
    @DisplayName("Test for equals() method.")
    void testEqualsContract() {
        EqualsVerifier.forClass(AccountOperation.class)
                .usingGetClass()
                .verify();
    }
}